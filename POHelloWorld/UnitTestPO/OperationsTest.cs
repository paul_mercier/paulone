﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POHelloWorld;

namespace UnitTestPO
{
    [TestClass]
    public class OperationsTest
    {
        [TestMethod]
        public void TestAdd()
        {
            Assert.AreEqual(12, Operations.add(5, 7));
        }
    }
}
